package com.expedia.imagestore.service;

import java.io.InputStream;

import javax.ws.rs.core.Response;

/**
 * Created by georgi.donkov on 17.4.2016 г..
 */
public interface ImageStoreService {

    Response storeImage(String name, InputStream inputStream);

    Response fetchImage(String name);

    Response size();

    Response clearAll();
}
