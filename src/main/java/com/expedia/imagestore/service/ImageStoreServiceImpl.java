package com.expedia.imagestore.service;

import com.expedia.imagestore.store.ImageStore;
import com.expedia.imagestore.store.ImageStoreExtended;
import com.expedia.imagestore.store.ImageStoreExtendedImpl;
import com.google.common.io.ByteStreams;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.core.Response;

/**
 * Created by georgi.donkov on 17.4.2016 г..
 */
public class ImageStoreServiceImpl implements ImageStoreService {

    private static final String STORE_PAGE = "<br/><a href=\"store\"># Store Images Page</a>";
    private static final String FETCH_PAGE = "<br/><a href=\"fetch\"># Fetch Images Page</a>";
    private static final String SIZE_PAGE = "<br/><a href=\"size\"># Count Images Page</a>";
    private static final String CLEAR_PAGE = "<br/><a href=\"clear\"># Clear Images Page</a>";
    private static final String MSG_TEMPLATE = "%s <br/><br/> %s %s %s %s";
    private static final String INVALID = "invalid";

    private static final ImageStoreExtended imageStore = new ImageStoreExtendedImpl();

    @Override
    public Response storeImage(String name, InputStream inputStream) {
        String message;
        String error;
        if (INVALID.equals(name)) {
            error = "Please select hotel name.";
            message = String.format(MSG_TEMPLATE, error, STORE_PAGE, FETCH_PAGE, SIZE_PAGE, CLEAR_PAGE);
            return Response.ok(message).build();
        }
        try {
            NonEmptyInputStream nonEmptyInputStream = new NonEmptyInputStream(inputStream);
            byte[] bytes = ByteStreams.toByteArray(nonEmptyInputStream);
            imageStore.storeImage(name, bytes);
            message = String.format(MSG_TEMPLATE, imageStore.printAll(), STORE_PAGE, FETCH_PAGE, SIZE_PAGE, CLEAR_PAGE);
            return Response.ok(message).build();
        } catch (NonEmptyInputStream.EmptyInputStreamException | IOException e) {
            error = "Failed: " + e.getMessage();
            message = String.format(MSG_TEMPLATE, error, STORE_PAGE, FETCH_PAGE, SIZE_PAGE, CLEAR_PAGE);
            return Response.ok(message).build();
        }
    }

    @Override
    public Response fetchImage(String name) {
        String message;
        String error;
        if (INVALID.equals(name)) {
            error = "Please select hotel name.";
            message = String.format(MSG_TEMPLATE, error, STORE_PAGE, FETCH_PAGE, SIZE_PAGE, CLEAR_PAGE);
            return Response.ok(message).build();
        }
        byte[] content = imageStore.fetchImage(name);
        if (content == null) {
            error = "Failed: The file does not exist!";
            message = String.format(MSG_TEMPLATE, error, STORE_PAGE, FETCH_PAGE, SIZE_PAGE, CLEAR_PAGE);
            return Response.ok(message).build();
        }
        File file = new File(name);
        try (FileOutputStream fis = new FileOutputStream(file)) {
            fis.write(content);
            return Response.ok(file).header("content-type", "image/jpg").build();
        } catch (IOException e) {
            e.printStackTrace();
            error = "Failed: " + e.getMessage();
            message = String.format(MSG_TEMPLATE, error, STORE_PAGE, FETCH_PAGE, SIZE_PAGE, CLEAR_PAGE);
            return Response.ok(message).build();
        }
    }

    @Override
    public Response size() {
        String message = String.format(MSG_TEMPLATE, imageStore.size(), STORE_PAGE, FETCH_PAGE, CLEAR_PAGE, "");
        return Response.ok(message).build();
    }

    @Override
    public Response clearAll() {
        String message = String.format(MSG_TEMPLATE, imageStore.clearAll(), STORE_PAGE, FETCH_PAGE, SIZE_PAGE, "");
        return Response.ok(message).build();
    }

}
