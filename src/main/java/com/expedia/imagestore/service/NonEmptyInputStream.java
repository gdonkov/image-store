package com.expedia.imagestore.service;

import com.google.common.base.Preconditions;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

/**
 * Created by georgi.donkov on 18.4.2016 г..
 */
public class NonEmptyInputStream extends FilterInputStream {
    /**
     * Once this stream has been created, do not consume the original InputStream as there will be one byte missing.
     * Consume that one instead.
     *
     * @param originalInputStream given {@link InputStream}
     * @throws IOException
     * @throws EmptyInputStreamException
     */
    public NonEmptyInputStream(InputStream originalInputStream) throws IOException, EmptyInputStreamException {
        super(checkStreamIsNotEmpty(originalInputStream));
    }

    /**
     * Checks if the InputStream is empty or not.
     *
     * @param inputStream the original input stream
     * @return in case of success returns input stream otherwise throws EmptyInputStreamException exception
     */
    private static InputStream checkStreamIsNotEmpty(InputStream inputStream) throws IOException,
            EmptyInputStreamException {
        Preconditions.checkArgument(inputStream != null, "The InputStream is mandatory");
        PushbackInputStream pushbackInputStream = new PushbackInputStream(inputStream);
        int b;
        b = pushbackInputStream.read();
        if (b == -1) {
            throw new EmptyInputStreamException("Could not read the file content. Please check if file was "
                    + "attached in the form.");
        }
        pushbackInputStream.unread(b);
        return pushbackInputStream;
    }

    /**
     * Exception that will be thrown if failed to read the input stream.
     */
    public static class EmptyInputStreamException extends RuntimeException {
        public EmptyInputStreamException(String message) {
            super(message);
        }
    }
}
