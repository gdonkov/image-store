package com.expedia.imagestore.resource;

import com.expedia.imagestore.resource.view.FetchImageView;
import com.expedia.imagestore.resource.view.StoreImageView;
import com.expedia.imagestore.service.ImageStoreService;
import com.expedia.imagestore.service.ImageStoreServiceImpl;
import com.sun.jersey.multipart.FormDataParam;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by georgi.donkov on 17.4.2016 г..
 */
@Path("/imageStore")
public class ImageStoreResource {

    private final ImageStoreService imageStoreService = new ImageStoreServiceImpl();

    @GET
    @Path("store")
    @Produces(MediaType.TEXT_HTML)
    public StoreImageView getStoreImageView() {
        return new StoreImageView();
    }

    @POST
    @Path("doStore")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response doUpload(@FormDataParam("file") InputStream inputStream,
            @FormDataParam("hotelName") String hotelName) {

        return imageStoreService.storeImage(hotelName, inputStream);

    }

    @GET
    @Path("fetch")
    @Produces(MediaType.TEXT_HTML)
    public FetchImageView getFetchImageView() {
        return new FetchImageView();
    }

    @GET
    @Path("size")
    @Produces(MediaType.TEXT_HTML)
    public Response getSize() {
        return imageStoreService.size();
    }

    @POST
    @Path("doFetch")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response doFetch(@FormDataParam("hotelName") String hotelName) {
        return imageStoreService.fetchImage(hotelName);
    }

    @GET
    @Path("clear")
    @Produces(MediaType.TEXT_HTML)
    public Response clearAll() {
        return imageStoreService.clearAll();
    }
}
