package com.expedia.imagestore.resource.view;

import io.dropwizard.views.View;

/**
 * Created by georgi.donkov on 17.4.2016 г..
 */
public class FetchImageView extends View {
    public FetchImageView() {
        super("fetch-image-view.ftl");
    }
}
