package com.expedia.imagestore.store;

import com.expedia.imagestore.store.ImageStore;

/**
 * Created by georgi.donkov on 18.4.2016 г..
 */
public interface ImageStoreExtended extends ImageStore {

    /**
     * Prints the status of both images and hotels all data structures.
     *
     * @return the data structures toString()
     */
    String printAll();

    /**
     * Clear the content of the data structure
     */
    String clearAll();
}
