package com.expedia.imagestore.store;

import com.expedia.imagestore.store.keeper.HotelKeeper;
import com.expedia.imagestore.store.keeper.ImageKeeper;

/**
 * Created by georgi.donkov on 16.4.2016 г..
 */
public class ImageStoreExtendedImpl implements ImageStoreExtended {

    @Override
    public synchronized void storeImage(String hotel, byte[] content) {
        HotelKeeper.saveHotel(hotel, ImageKeeper.saveImage(content));
        printAll();
    }

    @Override
    public synchronized byte[] fetchImage(String hotel) {
        return ImageKeeper.getImage(HotelKeeper.getImageId(hotel));
    }

    @Override
    public synchronized String size() {
        return ImageKeeper.getSize() + "<br/>" + HotelKeeper.getSize();
    }

    @Override
    public synchronized String printAll() {
        return ImageKeeper.print() + "<br/>" + HotelKeeper.print();
    }

    public synchronized String clearAll() {
        return ImageKeeper.clear() + "<br/>" + HotelKeeper.clear();
    }

}

