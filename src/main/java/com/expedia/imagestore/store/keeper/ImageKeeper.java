package com.expedia.imagestore.store.keeper;

import com.expedia.imagestore.store.hash.HashGenerator;
import com.expedia.imagestore.store.hash.HashGeneratorSRC32Impl;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by georgi.donkov on 17.4.2016 г..
 */
public class ImageKeeper {
    private static final Map<String, byte[]> images = new HashMap<>();
    private static final HashGenerator hashGenerator = new HashGeneratorSRC32Impl();

    public static String saveImage(byte[] image) {

        String checkSum = hashGenerator.hash(image);

        if (!images.containsKey(checkSum)) {
            images.put(checkSum, image);

        }
        return checkSum;
    }

    public static void removeImage(String checksum) {
        images.remove(checksum);
    }

    public static byte[] getImage(String imageKey) {
        return images.get(imageKey);
    }

    public static String getSize() {
        return "images:" + images.size();
    }

    public static String print() {
        return "images: " + images.toString();
    }

    public static String clear() {
        images.clear();
        return "Images data cleared.";
    }
}
