package com.expedia.imagestore.store.keeper;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by georgi.donkov on 17.4.2016 г..
 */
public class HotelKeeper {
    private static final Map<String, String> hotels = new HashMap<>();

    public static void saveHotel(String name, String newImageId) {
        // remove old references if present
        String oldImageKey = hotels.remove(name);

        if (!hotels.values().contains(oldImageKey)) {
            // if there is no more references to that image in the hotels Map
            // the image is to be removed from the ImageKeeper
            ImageKeeper.removeImage(oldImageKey);
        }
        hotels.put(name, newImageId);
    }

    public static String getImageId(String hotelName) {
        return hotels.get(hotelName);
    }

    public static String getSize() {
        return "hotels: " + hotels.size();
    }

    public static String print() {
        return "hotels: " + hotels.toString();
    }

    public static String clear() {
        hotels.clear();
        return "Hotels data cleared.";
    }
}
