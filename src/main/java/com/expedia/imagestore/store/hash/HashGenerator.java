package com.expedia.imagestore.store.hash;

/**
 * Created by georgi.donkov on 18.4.2016 г..
 */
public interface HashGenerator {

    String hash(byte[] content);
}
