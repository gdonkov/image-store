package com.expedia.imagestore.store.hash;

import java.util.zip.CRC32;
import java.util.zip.Checksum;

/**
 * Generates CRC32 checksum of each file content that can be used then for a key in the HashMap
 *
 * Created by georgi.donkov on 17.4.2016 г..
 */
public class HashGeneratorSRC32Impl implements HashGenerator {


    @Override
    public String hash(byte[] content) {
        Checksum checksum = new CRC32();
        checksum.update(content, 0, content.length);
        return String.valueOf(checksum.getValue());
    }
}
