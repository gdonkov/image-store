package com.expedia.imagestore.store;

/**
 * Created by georgi.donkov on 17.4.2016 г..
 */
public interface ImageStore {
    /**
     * Inserts an image in the store
     *
     * @param id -- The identifier of the image
     * @param content -- The content of the image
     */
    void storeImage(String id, byte[] content);

    /**
     * Retrieves an image from the store
     *
     * @param id -- The identifier of the image to be retrieved
     * @return the image content
     */
    byte[] fetchImage(String id);

    /**
     * The size of the store
     *
     * @return the actual store size
     */
    String size();

}
