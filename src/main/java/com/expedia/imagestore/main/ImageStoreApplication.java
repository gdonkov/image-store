package com.expedia.imagestore.main;

import com.expedia.imagestore.healthcheck.DummyHealthcheck;
import com.expedia.imagestore.resource.ImageStoreResource;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;

/**
 * Created by georgi.donkov on 17.4.2016 г..
 */
public class ImageStoreApplication extends Application<ImageStoreApplicationConfiguration> {

    private static final String HEALTH_CHECK_NAME = "dummy";
    private ImageStoreApplicationConfiguration configuration;
    private Environment environment;
    private ImageStoreResource imageStoreResource;
    private DummyHealthcheck healthcheck;

    @Override
    public void initialize(Bootstrap<ImageStoreApplicationConfiguration> bootstrap) {
        bootstrap.addBundle(new ViewBundle());
    }

    @Override
    public void run(ImageStoreApplicationConfiguration configuration, Environment environment) throws Exception {
        this.configuration = configuration;
        this.environment = environment;
        this.imageStoreResource = new ImageStoreResource();
        this.healthcheck = new DummyHealthcheck();

        environment.jersey().register(imageStoreResource);
        environment.healthChecks().register(HEALTH_CHECK_NAME, healthcheck);

    }

    /**
     * Main method for starting the application.
     *
     * @param args the arguments
     * @throws Exception if the application could not be started
     */
    public static void main(String[] args) throws Exception {
        new ImageStoreApplication().run(args);
    }

}
