package com.expedia.imagestore.healthcheck;

import com.codahale.metrics.health.HealthCheck;

/**
 * Dummy health check to make DropWizard stop complaining
 *
 * Created by georgi.donkov on 17.4.2016 г..
 */
public class DummyHealthcheck extends HealthCheck {
    @Override
    protected Result check() throws Exception {
        return null;
    }
}
