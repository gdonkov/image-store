<#-- @ftlvariable name="" type="com.expedia.imagestore.resource.view.StoreImageView" -->
       <html>
       <head/>
       <body>
       <h3>Store Images Page</h3>
       <div style="background:#b2e2b4; width: 320px; height: 135px; padding: 10px">
           <form enctype="multipart/form-data" method="POST" action="doStore">
               <fieldset style="width: 290px; height: 115px;">
                   <legend>Store Image Form</legend>
                   <input type="file" id="fileUpload" name="file"/><br/><br/>
                   <input type="hidden" id="fileName" name="fileName"/>
                   <select name="hotelName">
                       <option value="invalid" selected>Please Select Hotel...</option>
                       <option value="SunshineHotels">Sunshine Hotels</option>
                       <option value="StormyHotels">Stormy Hotels</option>
                       <option value="Beachfront Hotels">Beachfront Hotels</option>
                   </select>
                   <br/><br/>
                   <input type="submit" value="Store Image File"/>
               </fieldset>
           </form>
       </div>
       <br/>
       <a href="fetch"># Fetch Images Page</a>
       <br/>
       <a href="size"># Count Images Page</a>
       <br/>
       <a href="clear"># Clear Images Page</a>
       </body>
       </html>