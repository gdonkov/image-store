<#-- @ftlvariable name="" type="com.expedia.imagestore.resource.view.FetchImageView" -->
<html>
<head/>
<body>
<h3>Fetch Images Page</h3>
<div style="background:#86b9ff; width: 320px; height: 135px; padding: 10px">
    <form enctype="multipart/form-data" method="POST" action="doFetch">
        <fieldset style="width: 290px; height: 115px;">
            <legend>Fetch Image Form</legend>
            <select name="hotelName">
                <option value="invalid" selected>Please Select Hotel...</option>
                <option value="SunshineHotels">Sunshine Hotels</option>
                <option value="StormyHotels">Stormy Hotels</option>
                <option value="Beachfront Hotels">Beachfront Hotels</option>
            </select>
            <br/><br/>
            <br/><br/>
            <input type="submit" value="Fetch Image File"/>
        </fieldset>
    </form>
</div>
<br/>
<a href="store"># Store Images Page</a>
<br/>
<a href="size"># Count Images Page</a>
<br/>
<a href="clear"># Clear Images Page</a>
</body>
</html>